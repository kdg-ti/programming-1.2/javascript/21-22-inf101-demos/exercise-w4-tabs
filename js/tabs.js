let tabs = []

function asTabs(node) {
   tabs = Array.from(node.children).map(node => {
    let button = document.createElement("button");
    // data-* attributes are attributes anybody can add
     // they can be accessed using normal getAttribute
     //button.textContent = node.getAttribute("data-tab-name");
     // or using the dataset attribute followed by an attribute
     // named after the part after data-
     // where every - (kebab-case) is removed and
     // the letter after it is upper case (camelCase)
    button.textContent = node.dataset.tabName;

    // we bundle the div (node) and the button in an object
     // because we often use them together

    let tab = {node, button};
    // the above is a shorthand notation for
    //let tab = {"node":node, "button":button};
     // you can also write the same as
    //  let tab ={}
    //  tab.node=node
    //  tab.button=button

     // we do not want to pass the event to the event handler
     // but the tab. So we pass a different parameter than event
     // in the lambda.
     // For every button the event handler will refer to the corresponding tab
     // because the lambda is a closure (remembers the tab variable in the function
     // where the lambda is defined)
     button.addEventListener("click", event => selectTab(tab));

     // returning the tab (containing button and node).
     // All tab objects returned by map will be stored in the tabs array
    return tab;
  });
}

function selectTab(selectedTab) {
  for (let tab of tabs) {
    let selected = tab == selectedTab;
    // setting all tabs to display=none except the selected tab
    tab.node.style.display = selected ? "" : "none";
    // making the selected button red and removing color from all other buttons
    tab.button.style.color = selected ? "red" : "";
  }
}

function init() {
  let node = document.getElementById("tab-panel")
  asTabs(node);
  let tabList = document.createElement("div");
  // for loop with a destructuring assignment
  // All variables between {} in the assignment
  // receive the value of the property with the same name
  for (let {button} of tabs) {
    tabList.appendChild(button);
  }

  // alternate syntax for doing the same
//  for (let tab of tabs) {
//    let button=tab.button
//    tabList.appendChild(button);
//  }
  node.insertBefore(tabList, node.firstChild);
  selectTab(tabs[0]);
}

init()